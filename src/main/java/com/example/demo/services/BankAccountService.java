package com.example.demo.services;

import java.math.BigDecimal;

public interface BankAccountService {
    String withdrawBalance(Long accountId, BigDecimal amount);

}
