package com.example.demo.services;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;


public interface NotificationService {
    String publishWithdrawalEvent(BigDecimal amount, Long accountId);
}
