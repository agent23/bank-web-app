package com.example.demo.services;

import com.example.demo.entities.Account;
import com.example.demo.repos.BankAccountRepo;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class BankAccountServiceImpl implements BankAccountService{

    private final JdbcTemplate jdbcTemplate;
    private final NotificationServiceImpl notificationService;


    public BankAccountServiceImpl(JdbcTemplate jdbcTemplate, NotificationServiceImpl notificationService) {
        this.jdbcTemplate = jdbcTemplate;
        this.notificationService = notificationService;
    }

    private BigDecimal getBalance(Long accountId) {
        String sql = "SELECT balance FROM Account WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{accountId}, BigDecimal.class);
    }

    @Override
    public String withdrawBalance(Long accountId, BigDecimal amount) {
        //Get account balance
        BigDecimal currentBalance = getBalance(accountId);

        if (currentBalance != null && currentBalance.compareTo(amount) >= 0) {
            // Update balance

            int rowsAffected = updateBalance(accountId, amount);
            if (rowsAffected > 0) {
                return notificationService.publishWithdrawalEvent(amount, accountId);
            } else { // In case the update fails for reasons other than a balance check
                return "Withdrawal failed";
            }
        } else {
            // Insufficient funds
            return "Insufficient funds for withdrawal";
        }
    }

    private Integer updateBalance(Long accountId, BigDecimal amount) {
        String sql = "UPDATE accounts SET balance = balance - ? WHERE id = ?";
        return jdbcTemplate.update(sql, amount, accountId);
    }

}
