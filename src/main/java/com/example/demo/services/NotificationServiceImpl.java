package com.example.demo.services;

import com.example.demo.dtos.WithdrawalEvent;
import com.example.demo.configurations.AWSConfigurations;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

import java.math.BigDecimal;

@Service
public class NotificationServiceImpl implements NotificationService{

    private SnsClient snsClient;
    private AWSConfigurations awsConfigurations;


    public NotificationServiceImpl(AWSConfigurations awsConfigurations) {
        this.snsClient = SnsClient.builder()
                .region(Region.AP_EAST_1)
                .build(); // Specify your region
        this.awsConfigurations = awsConfigurations;
    }

    @Override
    public String publishWithdrawalEvent(BigDecimal amount, Long accountId) {
        WithdrawalEvent event = new WithdrawalEvent(amount, accountId, "SUCCESSFUL");
        String eventJson = event.toJson();
        // Convert event to JSON
        String snsTopicArn = awsConfigurations.getArn();
        PublishRequest publishRequest = PublishRequest.builder()
                                                .message(eventJson)
                                                .topicArn(snsTopicArn)
                                                .build();

        PublishResponse publishResponse = snsClient.publish(publishRequest);

        if  (publishResponse.sdkHttpResponse().isSuccessful()){
            return "Message Published";
        }else {
            return "Publish Event Failed";
        }
    }
}
